package com.omega.paging.listener;

import com.omega.paging.entity.PagedObject;

/**
 *
 * @author Kyu
 * @param <T>
 */
public interface PagedObjectListener<T extends PagedObject> {
    
    public void onInitialized(T object);
    public void onAdded(T  object);
    public void onRemoved(T object);
    public void onDestroyed(T object);
}
