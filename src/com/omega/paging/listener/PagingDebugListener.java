package com.omega.paging.listener;

import com.omega.paging.PagingPatch;
import com.omega.paging.entity.PagedObject;

/**
 *
 * @author Kyu
 * @param <T>
 */
public interface PagingDebugListener<T extends PagedObject> {
    
    public void onPatchChange(PagingPatch<T> patch); 
}
