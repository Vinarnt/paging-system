package com.omega.paging.listener;

import com.omega.paging.PagingPatch;
import com.omega.paging.entity.PagedObject;

/**
 *
 * @author Kyu
 * @param <T>
 */
public interface PatchListener<T extends PagedObject> {
    
    public void onInitialized(PagingPatch<T> patch);
    public void onAdded(PagingPatch<T> patch);
    public void onRemoved(PagingPatch<T> patch);
    public void onDestroyed(PagingPatch<T> patch);
}
