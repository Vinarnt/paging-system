package com.omega.paging;

/**
 *
 * @author Kyu
 */
public enum PagingState {
    UNKNOWN, INITIALIZING, INITIALIZED, ACTIVE, SLEEPING, LOCKED, DESTROYED;
}
