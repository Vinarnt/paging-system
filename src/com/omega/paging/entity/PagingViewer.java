package com.omega.paging.entity;

/**
 *
 * @author Kyu
 */
public abstract class PagingViewer<T> extends ExternalObject<T> {

    public PagingViewer(T object) {
        super(object);
    }
}
