package com.omega.paging.entity;

import com.omega.paging.util.Vector3f;

/**
 *
 * @author Kyu
 */
public abstract class ExternalObject<T> {
    
    protected T object;
    protected Vector3f tempVect = new Vector3f();

    public ExternalObject() {
    }
    
    public ExternalObject(T object) {
        this.object = object;
    }
    
    /**
     * Store the position of your PagedObject in tempVect.
     * 
     * @return the position of the {@link PagedObject object}.
     */
    public abstract Vector3f getPosition();
    
    public T getObject()
    {
        return object;
    }
}
