package com.omega.paging.entity;

import com.omega.paging.PagingState;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Kyu
 */
public abstract class PagedObject<T> extends ExternalObject<T> {

    private PagingState state = PagingState.UNKNOWN;
    private Set<PagingViewer> lockedBy = new HashSet<PagingViewer>();

    public PagedObject(T object) {
        super(object);
    }

    public void setState(PagingState state) {
        this.state = state;
    }

    public PagingState getState() {
        return state;
    }

    public void addLock(PagingViewer viewer) {
        lockedBy.add(viewer);
    }

    public void removeLock(PagingViewer viewer) {
        lockedBy.remove(viewer);
    }

    public boolean isLockedBy(PagingViewer viewer) {
        return lockedBy.contains(viewer);
    }

    public int getLockCount() {
        return lockedBy.size();
    }
}
