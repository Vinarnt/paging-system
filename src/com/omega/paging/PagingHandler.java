package com.omega.paging;

import com.omega.paging.entity.PagingViewer;
import com.omega.paging.entity.PagedObject;
import com.omega.paging.listener.PagedObjectListener;
import com.omega.paging.listener.PagingDebugListener;
import com.omega.paging.listener.PatchListener;
import com.omega.paging.util.MathUtil;
import com.omega.paging.util.Vector3f;
import com.omega.paging.util.Vector3;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *  Handle all {@link PagingPatch patches} and {@link PagingViewer viewers} for the specified {@link PagedObject paged objects}.
 * 
 * @param <T> The {@link PagedObject paged object} type this {@link PagingHandler paging handler} handle.
 * @author Kyu
 */
public class PagingHandler<T extends PagedObject> {

    private static final Logger LOGGER = Logger.getLogger(PagingHandler.class.getName());
    private float viewDistance;
    private Map<PagingViewer, Set<PagingPatch<T>>> activePatches = new ConcurrentHashMap<PagingViewer, Set<PagingPatch<T>>>();
    private Map<Vector3, PagingPatch<T>> cachedPatches = new ConcurrentHashMap<Vector3, PagingPatch<T>>();
    private List<PagingPatch<T>> toActiveList = new ArrayList<PagingPatch<T>>();
    private List<PagingPatch<T>> toRemoveList = new ArrayList<PagingPatch<T>>();
    private List<PatchListener<T>> patchListeners = new ArrayList<PatchListener<T>>();
    private List<PagedObjectListener<T>> pagedObjectListeners = new ArrayList<PagedObjectListener<T>>();
    private List<PagingDebugListener<T>> debugListeners = new ArrayList<PagingDebugListener<T>>();
    private Map<PagingViewer, PagingPatch<T>> currentPatches = new HashMap<PagingViewer, PagingPatch<T>>();
    private Set<PagingPatch<T>> activeAreaPatches = new HashSet<PagingPatch<T>>(27);
    private Future future;
    private int activeCount;

    /**
     *
     * @param viewDistance
     */
    public PagingHandler(float viewDistance) {
        LOGGER.log(Level.FINE, "Create PagingHandler with view distance of {0}", viewDistance);

        this.viewDistance = viewDistance;
    }

    /**
     * Called internally.
     */
    public void update() {
        for (PagingViewer viewer : PagingManager.getInstance().getViewers()) {
            
            // Handle patch change listener
            final PagingPatch<T> oldCurrentPatch = currentPatches.get(viewer);
            final PagingPatch<T> currentPatch = getPatchAtWorldCoord(viewer.getPosition());
            
            currentPatches.put(viewer, currentPatch);
            boolean needUpdate = false;
            
            if (oldCurrentPatch != currentPatch || currentPatch.getState() == PagingState.SLEEPING) {
                needUpdate = true;
                for (PagingDebugListener<T> listener : debugListeners) {
                    listener.onPatchChange(currentPatch);
                }
            }

            // Get the set of activePatches for this viewer
            Set<PagingPatch<T>> activesPatch = activePatches.get(viewer);
            if (activesPatch == null) {
                activesPatch = activePatches.put(viewer, new HashSet<PagingPatch<T>>());
            }

            Set<PagingPatch<T>> neighbours;
            if (needUpdate) { // Need to update active patches for this viewer                
                // Initialize patches to remove
                toRemoveList.clear();
                toRemoveList.addAll(activesPatch);

                // Get new active patches
                neighbours = getPatchesArea(currentPatch);

                // Remove new active patches and retain the old active patches to remove now
                toRemoveList.removeAll(neighbours);

                toActiveList.clear();
                toActiveList.addAll(neighbours);

                // Activate new active patches
                toActiveList.removeAll(activesPatch);
                for (PagingPatch<T> toActive : toActiveList) {
                    setPatchState(toActive, PagingState.ACTIVE);
                }

                // Remove old patches
                for (PagingPatch<T> toRemovePatch : toRemoveList) {
                    setPatchState(toRemovePatch, PagingState.SLEEPING);
                }
                
                // Set new patches
                activesPatch.clear();
                activesPatch.addAll(neighbours);
            } else {
                neighbours = activesPatch;
            }

            // Update active patches
            for (PagingPatch<T> neighbour : neighbours) {
                neighbour.update(viewer);
            }
        }
    }

    /**
     * Called internally.
     */
    public void cleanup() {
        if (future != null) {
            future.cancel(true);
        }
        for (PagingPatch<T> patch : cachedPatches.values()) {
            setPatchState(patch, PagingState.DESTROYED);
        }
    }

    /**
     * Get the {@link PagingPatch} at the specified world coordinates. If no one
     * is found, a new one is created.
     *
     * @param x the x axis
     * @param y the y axis
     * @param z the z axis
     * @return the patch at the specified world position
     */
    public PagingPatch<T> getPatchAtWorldCoord(float x, float y, float z) {
        return getPatchAtPatchCoord(MathUtil.worldToPatchCoord(viewDistance, x, y, z));
    }

    /**
     * Get the {@link PagingPatch} at the specified world coordinates. If no one
     * is found, a new one is created.
     *
     * @param position the world position
     * @return the patch at the specified world position
     */
    public PagingPatch<T> getPatchAtWorldCoord(Vector3f position) {
        return getPatchAtPatchCoord(MathUtil.worldToPatchCoord(viewDistance, position));
    }

    /**
     * Get the {@link PagingPatch} at the specified patch coordinates. 1 unit is
     * equal to the patch size. If no one is found, a new one is created.
     *
     * @param patchCoord the position of the patch to get
     * @return the patch at the specified patch position
     */
    public PagingPatch<T> getPatchAtPatchCoord(final Vector3 patchCoord) {
        if (patchCoord == null) {
            throw new IllegalArgumentException("Patch coordinates should not be null");
        }
        PagingPatch patch = cachedPatches.get(patchCoord);
        if (patch == null) // Create one
        {
            try {
                Vector3 vector = (Vector3) patchCoord.clone();
                patch = new PagingPatch(this, vector);
                setPatchState(patch, PagingState.INITIALIZING);
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(PagingHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return patch;
    }

    /**
     * Get all {@link PagingPatch patches} around one {@link PagingPatch patch}.
     *
     * @param refPatch the center patch of reference
     * @return an array of the 27 patches for the area
     */
    public Set<PagingPatch<T>> getPatchesArea(PagingPatch<T> refPatch) {
        final Vector3 coordVect = new Vector3(refPatch.getLocation());

        activeAreaPatches.clear();
        // Top
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addY(1))); // Center
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1))); // West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addX(1).substractZ(1))); // North
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addZ(1).addX(1))); // East
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1).addZ(1))); // South
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1))); // South West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractZ(2))); // North West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addX(2))); // North East
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addZ(2))); // South East

        // Middle        
        activeAreaPatches.add(refPatch); // Center
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractY(1).substractX(2).substractZ(1))); // West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addX(1).substractZ(1))); // North
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addZ(1).addX(1))); // East
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1).addZ(1))); // South
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1))); // South West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractZ(2))); // North West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addX(2))); // North East
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addZ(2))); // South East

        // Bottom
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractY(1).substractX(1).substractZ(1))); // Center
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1))); // West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addX(1).substractZ(1))); // North
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addZ(1).addX(1))); // East
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1).addZ(1))); // South
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractX(1))); // South West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.substractZ(2))); // North West
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addX(2))); // North East
        activeAreaPatches.add(getPatchAtPatchCoord(coordVect.addZ(2))); // South East

        return activeAreaPatches;
    }

    /**
     * Change the {@link PagingState state} of a {@link PagingPatch}.
     *
     * @param patch the patch to change state
     * @param state the state to set
     */
    public void setPatchState(PagingPatch<T> patch, PagingState state) {
        patch.setState(state);
        switch (state) {
            case INITIALIZING:
                cachedPatches.put(patch.getLocation(), patch);
                setPatchState(patch, PagingState.INITIALIZED);
                break;

            case INITIALIZED:
                for (PatchListener<T> listener : patchListeners) {
                    listener.onInitialized(patch);
                }
                activeCount++;
                setPatchState(patch, PagingState.SLEEPING);

                break;

            case ACTIVE:
                patch.addLock();
                if (patch.getLockCount() == 1) {
                    activeCount++;
                    for (PatchListener<T> listener : patchListeners) {
                        listener.onAdded(patch);
                    }
                }
                break;

            case SLEEPING:
                patch.removeLock();
                if (patch.getLockCount() == 0) {
                    activeCount--;
                    for (PatchListener<T> listener : patchListeners) {
                        listener.onRemoved(patch);
                    }
                }
                break;

            case DESTROYED:
                cachedPatches.remove(patch.getLocation());
                for (PatchListener<T> listener : patchListeners) {
                    listener.onDestroyed(patch);
                }
                break;
        }

    }

    /**
     * Get the view distance of this {@link PagingHandler handler}.
     * 
     * @return the handler view distance
     */
    public float getViewDistance() {
        return viewDistance;
    }

    /**
     * Get the future of this {@link PagingHandler handler}.
     * 
     * @return the handler future
     */
    public Future getFuture() {
        return future;
    }

    /**
     * Called internally.
     * 
     * @param future the future of this handler
     */
    public void setFuture(Future future) {
        this.future = future;
    }

    /**
     * Add a {@link PagedObject paged object} to this {@link PagingHandler paging handler}.
     * 
     * @param object the paged object to add
     */
    public void addPaggedObject(T object) {
        getPatchAtWorldCoord(object.getPosition()).addPagedObject(object);
    }

    /**
     * Remove a {@link PagedObject paged object} from this {@link PagingHandler paging handler}.
     * 
     * @param object the paged object to remove
     */
    public void removePaggedObject(T object) {
        getPatchAtWorldCoord(object.getPosition()).removePagedObject(object);
    }

    /**
     * Add a {@link PagedObjectListener paged object listener} to this {@link PagingHandler paging handler}.
     * 
     * @param listener the listener to add
     */
    public void addListener(PagedObjectListener<T> listener) {
        pagedObjectListeners.add(listener);
    }

    /**
     * Remove a {@link PagedObjectListener paged object listener} from this {@link PagingHandler paging handler}.
     * 
     * @param listener the listener to remove
     */
    public void removeListener(PagedObjectListener<T> listener) {
        pagedObjectListeners.remove(listener);
    }

    /**
     * Get the {@link PagedObjectListener paged object listeners} of this {@link PagingHandler handler}.
     * 
     * @return the paged object listeners
     */
    public List<PagedObjectListener<T>> getPagedObjectListeners() {
        return pagedObjectListeners;
    }

    /**
     * Add a {@link PatchListener patch listener} to this {@link PagingHandler handler}.
     * 
     * @param listener the listener to add
     */
    public void addListener(PatchListener listener) {
        patchListeners.add(listener);
    }

    /**
     * Remove a {@link PatchListener patch listener} from this {@link PagingHandler handler}.
     * 
     * @param listener the listener to remove
     */
    public void removeListener(PatchListener listener) {
        patchListeners.remove(listener);
    }

    /**
     * Get the {@link PatchListener patchlisteners} of this {@link PagingHandler handler}.
     * 
     * @return the patch listeners 
     */
    public List<PatchListener<T>> getPatchListeners() {
        return patchListeners;
    }

    /**
     * Add a {@link PagingDebugListener debug listener} to this {@link PagingHandler handler}.
     * 
     * @param listener the listener to add
     */
    public void addListener(PagingDebugListener listener) {
        debugListeners.add(listener);
    }

    /**
     * Remove a {@link PagingDebugListener debug listener} from this {@link PagingHandler handler}.
     * 
     * @param listener the listener to remove
     */
    public void removeListener(PagingDebugListener listener) {
        debugListeners.remove(listener);
    }

    /**
     * Get the {@link PagingDebugListener debug listeners} of this {@link PagingHandler handler}.
     * 
     * @return the debug listeners
     */
    public List<PagingDebugListener<T>> getDebugListeners() {
        return debugListeners;
    }
}
