package com.omega.paging.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class Vector3 implements Cloneable {

    private int x;
    private int y;
    private int z;

    public Vector3() {
    }

    public Vector3(Vector3 vector) {
        this(vector.x, vector.y, vector.z);
    }

    public Vector3(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Vector3 setX(int x) {
        this.x = x;

        return this;
    }

    public Vector3 setY(int y) {
        this.y = y;

        return this;
    }

    public Vector3 setZ(int z) {
        this.z = z;

        return this;
    }

    public Vector3 addX(int x) {
        this.x += x;

        return this;
    }

    public Vector3 addY(int y) {
        this.y += y;

        return this;
    }

    public Vector3 addZ(int z) {
        this.z += z;

        return this;
    }

    public Vector3 substractX(int x) {
        this.x -= x;

        return this;
    }

    public Vector3 substractY(int y) {
        this.y -= y;

        return this;
    }

    public Vector3 substractZ(int z) {
        this.z -= z;

        return this;
    }

    @Override
    public int hashCode() {
        int hashcode = 23;
        hashcode = hashcode * 31 + x;
        hashcode = hashcode * 31 + y;
        hashcode = hashcode * 31 +z;
        
        return hashcode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vector3 other = (Vector3) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        if (this.z != other.z) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ", " + z + "]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException  {
        Object clone = null;
        try {
            clone = super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Vector3.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return clone;
    }
    
}
