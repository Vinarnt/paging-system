package com.omega.paging.util;

/**
 *
 * @author Kyu
 */
public final class MathUtil {
    
    /**
     * Get the distance between two 3D points.
     * 
     * @param first the first point
     * @param second the second point
     * @return the ditance between the two points
     */
    public static float getDistanceBetween(Vector3f first, Vector3f second)
    {
        double distance;
        
        distance = Math.sqrt(Math.pow(first.getX() - second.getX(), 2f) + 
                Math.pow(first.getY() - second.getY(), 2f) +
                Math.pow(first.getZ() - second.getZ(), 2f));
        
        return (float) distance;
    }
    
       /**
     * Convert world position to patch position.
     *
     * @param viewdistance
     * @param worldCoord
     * @return the patch position
     */
    public static Vector3 worldToPatchCoord(float viewdistance, Vector3f worldCoord) {
        return worldToPatchCoord(viewdistance, worldCoord.getX(), worldCoord.getY(), worldCoord.getZ());
    }

    /**
     * Convert world position to patch position.
     *
     * @param viewDistance
     * @param x
     * @param y
     * @param z
     * @return the patch position
     */
    public static Vector3 worldToPatchCoord(float viewDistance, float x, float y, float z) {
        int patchX = (int) Math.ceil(x / viewDistance);
        int patchY = (int) Math.ceil(y / viewDistance);
        int patchZ = (int) Math.ceil(z / viewDistance);

        return new Vector3(patchX, patchY, patchZ);
    }
}