package com.omega.paging.util;

/**
 *
 * @author Kyu
 */
public class Vector3f {

    private float x;
    private float y;
    private float z;

    public Vector3f() {
    }

    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public Vector3f setX(float x) {
        this.x = x;
        
        return this;
    }

    public Vector3f setY(float y) {
        this.y = y;
        
        return this;
    }

    public Vector3f setZ(float z) {
        this.z = z;
        
        return this;
    }
    
    public void setArray(float[] array)
    {
        if(array == null )
            throw new IllegalArgumentException("The array should not be null.");
        if(array.length < 3)
            throw  new IllegalArgumentException("The array should have a length of 3.");
        
        x = array[0];
        y = array[1];
        z = array[2];
    }
    
}
