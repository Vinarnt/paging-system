package com.omega.paging;

import static com.omega.paging.PagingState.ACTIVE;
import static com.omega.paging.PagingState.DESTROYED;
import static com.omega.paging.PagingState.INITIALIZED;
import static com.omega.paging.PagingState.INITIALIZING;
import static com.omega.paging.PagingState.SLEEPING;
import com.omega.paging.entity.PagedObject;
import com.omega.paging.entity.PagingViewer;
import com.omega.paging.listener.PagedObjectListener;
import com.omega.paging.util.MathUtil;
import com.omega.paging.util.Vector3;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Store {@link PagedObject paged objects} of a definite area and update them
 * with the {@link PagingViewer viewers}.
 *
 * @param <T> The {@link PagedObject paged objects} handled by this
 * {@link PagingPatch patch}
 * @author Kyu
 */
public class PagingPatch<T extends PagedObject> {

    protected static final Logger LOGGER = Logger.getLogger(PagingPatch.class.getName());
    protected PagingHandler<T> parent;
    protected final Vector3 location;
    protected PagingState state = PagingState.UNKNOWN;
    protected short lockCount;
    protected List<T> pagedObjects = new ArrayList<T>();
    protected Object userData;

    /**
     *
     * @param parent
     * @param location
     */
    public PagingPatch(PagingHandler<T> parent, Vector3 location) {
        this.parent = parent;
        this.location = location;
    }

    /**
     * Called internally.
     * 
     * @param viewer the viewer that update the patch
     */
    public void update(PagingViewer viewer) {
        for (T pagedObject : pagedObjects) {
            float distance = MathUtil.getDistanceBetween(viewer.getPosition(), pagedObject.getPosition());

            if (distance >= parent.getViewDistance()) {
                pagedObject.removeLock(viewer);
                if (pagedObject.getLockCount() == 0) {
                    setPagedObjectState(pagedObject, PagingState.SLEEPING);
                }
            } else {
                pagedObject.addLock(viewer);
                if (pagedObject.getLockCount() == 1) {
                    setPagedObjectState(pagedObject, PagingState.ACTIVE);
                }
            }
        }
    }

    /**
     *
     * @param object
     */
    public void addPagedObject(T object) {
        setPagedObjectState(object, PagingState.INITIALIZING);
    }

    /**
     *
     * @param object
     */
    public void removePagedObject(T object) {
        setPagedObjectState(object, PagingState.DESTROYED);
    }

    /**
     *
     * @param pagedObject
     * @param state
     */
    public void setPagedObjectState(T pagedObject, PagingState state) {
        if (pagedObject.getState() == state) {
            return;
        }

        pagedObject.setState(state);
        switch (state) {
            case INITIALIZING:
                pagedObjects.add(pagedObject);
                setPagedObjectState(pagedObject, PagingState.INITIALIZED);

                break;

            case INITIALIZED:
                for (PagedObjectListener<T> objectListener : parent.getPagedObjectListeners()) {
                    objectListener.onInitialized(pagedObject);
                }
                setPagedObjectState(pagedObject, SLEEPING);

                break;

            case ACTIVE:
                for (PagedObjectListener<T> listener : parent.getPagedObjectListeners()) {
                    listener.onAdded(pagedObject);
                }

                break;

            case SLEEPING:
                for (PagedObjectListener<T> listener : parent.getPagedObjectListeners()) {
                    listener.onRemoved(pagedObject);
                }

                break;

            case DESTROYED:
                pagedObjects.remove(pagedObject);
                for (PagedObjectListener<T> listener : parent.getPagedObjectListeners()) {
                    listener.onDestroyed(pagedObject);
                }

                break;
        }
    }

    /**
     *
     * @return
     */
    public List<T> getPagedObjects() {
        return pagedObjects;
    }

    /**
     *
     * @param parent
     */
    public void setParent(PagingHandler parent) {
        this.parent = parent;
    }

    /**
     *
     * @return
     */
    public PagingHandler getParent() {
        return parent;
    }

    /**
     *
     * @param state
     */
    public void setState(PagingState state) {
        this.state = state;
    }

    /**
     *
     * @return
     */
    public PagingState getState() {
        return state;
    }

    /**
     *
     * @return
     */
    public Vector3 getLocation() {
        return location;
    }

    /**
     *
     * @param userData
     */
    public void setUserData(Object userData) {
        this.userData = userData;
    }

    /**
     *
     * @return
     */
    public Object getUserData() {
        return userData;
    }

    /**
     *
     * @return
     */
    public short getLockCount() {
        return lockCount;
    }

    /**
     *
     */
    public void addLock() {
        lockCount++;
    }

    /**
     *
     */
    public void removeLock() {
        if ((lockCount - 1) < 0) {
            return;
        }

        lockCount--;
    }

    /**
     * Get the string representation of this {@link PagingPatch patch}. It's
     * basically it's position in patch coordinates.
     *
     * @return the string patch representation
     */
    @Override
    public String toString() {
        return location.toString();
    }
}
