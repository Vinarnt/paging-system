package com.omega.paging;

import com.omega.paging.entity.PagingViewer;
import com.omega.paging.entity.PagedObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manage all {@link PagingHandler handlers} and {@link PagingViewer viewers}.
 * 
 * @author Kyu
 */
public class PagingManager {

    /**
     * Logger for this class.
     */
    protected static final Logger LOGGER = Logger.getLogger(PagingManager.class.getName());
    
    /**
     * Store the {@link PagingHandler paging handlers} by {@link PagedObject paged object} types.
     */
    protected Map<Class<? extends PagedObject>, PagingHandler<PagedObject>> handlers =
            new HashMap<Class<? extends PagedObject>, PagingHandler<PagedObject>>();
    
    /**
     * Store the registered {@link PagingViewer paging viewers}.
     */
    protected List<PagingViewer> viewers = new ArrayList<PagingViewer>();
    
    /**
     * Executor for the {@link PagingHandler paging handlers} thread pool.
     */
    protected ExecutorService executor;
    
    /**
     * Singleton.
     */
    private static PagingManager instance;

    private PagingManager() {
        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    /**
     * Get the singleton instance of this class.
     * 
     * @return the instance of this class
     */
    public static PagingManager getInstance() {
        if (instance == null) {
            instance = new PagingManager();
        }

        return instance;
    }

    /**
     * Add a {@link PagingHandler} for the specified {@link PagedObject type}.
     * 
     * @param type the type of PagedObject handled by this handler
     * @param handler the handler handling this type of PagedObject
     * @return the paging handler added
     */
    public PagingHandler addHandler(Class<? extends PagedObject> type, PagingHandler handler) {
        LOGGER.log(Level.FINE, "Add handler of type {0}", type.getSimpleName());
        
        handlers.put(type, handler);
        
        return handler;
    }

    /**
     * Remove the {@link PagingHandler} for this type of {@link PagedObject}.
     * 
     * @param type the PagedObject handler type to remove
     * @return the paging handler removed
     */
    public PagingHandler removeHandler(Class<? extends PagedObject> type) {
        LOGGER.log(Level.FINE, "Remove handler of type {0}", type.getSimpleName());
        
        return handlers.remove(type);
    }

    /**
     * Get the {@link PagingHandler} for the specified type of
     * {@link PagedObject}.
     *
     * @param <T> the paged object type
     * @param type the paged object type of the handler
     * @return The handler for the specified type or null if no handler was
     * found
     */
    public <T extends PagedObject> PagingHandler<T> getHandler(Class<T> type) {
        PagingHandler<T> handler = (PagingHandler<T>) handlers.get(type);
        if (handler == null) {
            LOGGER.log(Level.WARNING, "Cannot get paging handler of type {0}", type.getSimpleName());
        }

        return handler;
    }

    /**
     * Get all registered {@link PagingViewer viewers}.
     * 
     * @return the registered viewers
     */
    public List<PagingViewer> getViewers() {
        return viewers;
    }

    /**
     * Register a {@link PagingViewer viewer}.
     * 
     * @param viewer the viewer to add
     */
    public void addViewer(PagingViewer viewer) {
        LOGGER.log(Level.FINE, "Add viewer");
        viewers.add(viewer);
    }

    /**
     * Unregister a {@link PagingViewer viewer}.
     * 
     * @param viewer the viewer to remove
     */
    public void removeViewer(PagingViewer viewer) {
        LOGGER.log(Level.FINE, "Remove viewer");
        viewers.remove(viewer);
    }

    /**
     * Called internally.
     */
    public void update() {
        for (final PagingHandler handler : handlers.values()) {
            if (handler.getFuture() != null && !handler.getFuture().isDone()) {
                return;
            }

            handler.setFuture(executor.submit(new Runnable() {
                @Override
                public void run() {
                    handler.update();
                }
            }));
        }
    }

    /**
     * Cleanup the PagingSystem.
     */
    public void cleanup() {
        executor.shutdownNow();
        for (PagingHandler handler : handlers.values()) {
            handler.cleanup();
        }
    }
}
